jquery.map
-----------------------------------  
  模拟java集合中的Map对象，方便用于存储key-value数据，使用方法如下： 

###  
	<script src="jquery.map-0.0.1.min.js"></script>

	<script type="text/javascript">
		$(function() {
			$.Map.put('A', 'CC');
			$.Map.put('B', 1);
			$.Map.put('C', true);
	
			alert("get方法：" + $.Map.get('B'));
			alert("containsKey方法：" + $.Map.containsKey('C'));
			alert("containsValue方法：" + $.Map.containsValue(2));
			alert("remove方法：" + $.Map.remove('C'));
			alert("size方法：" + $.Map.size());
			
			alert("keys方法：" + $.Map.keys());
			alert("values方法：" + $.Map.values());
			alert("清空前调用isEmpty方法：" + $.Map.isEmpty());
			alert("clear方法：" + $.Map.clear());
			alert("清空后调用isEmpty方法：" + $.Map.isEmpty());
		});
	</script>

###

方法说明
-----------------------------------  
* $.Map.put 向Map中设置值
* $.Map.get 根据key从Map中获取值
* $.Map.size 获取Map中元素个数 
* $.Map.remove 根据key删除Map中元素
* $.Map.isEmpty 判断map是否为空，如果为空则返回true，否则返回false
* $.Map.containsKey 判断map中是否存在指定的key，如果存在返回true，否则返回false
* $.Map.containsValue 判断map中是否存在指定的value，如果存在返回true，否则返回false
* $.Map.clear 清空map中所有元素
* $.Map.keys 遍历map中所有key，返回所有key组成的数组
* $.Map.values 遍历map中所有value，返回所有value组成的数组